var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var company = require('./routes/company_routes'); // STOP HERE #1
var address = require('./routes/address_routes'); // Lab 8 Address
var school = require('./routes/school_routes'); // Lab 8 School
var skill = require('./routes/skill_routes'); // Lab 8 Skill
var account = require('./routes/account_routes'); // Lab 8 Account
var resume = require('./routes/resume_routes'); // Lab 9 resume

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/company', company); // STOP HERE #2
app.use('/address', address); // Lab 8 Address CHECK index.ejs!!!
app.use('/school', school); // Lab 8 School
app.use('/skill', skill); // Lab 8 Skill
app.use('/account', account); // Lab 8 Account
app.use('/resume', resume); // Lab 9 resume

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

var express = require('express');
var router = express.Router();

var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var skill_dal = require('../model/skill_dal');
var company_dal = require('../model/company_dal');
var school_dal = require('../model/school_dal');

// View All accounts //
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result':result[0], 'skills':result[1], 'companies':result[2], 'schools':result[3]});
            }
        });
    }
});

// Select a user to create a new resume for
router.get('/add/selectuser', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumePickUser', {'accounts': result});
        }
    });
});

// add a new resume
router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    if (req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        skill_dal.userGet(req.query.account_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                company_dal.userGet(req.query.account_id, function (err, comResult) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        school_dal.userGet(req.query.account_id, function (err, schResult) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('resume/resumeAddForUser', {'account': req.query.account_id, 'skills': result, 'companies': comResult, 'schools': schResult});
                            }

                        });
                    }
                });
            }
        });
    }
});

router.post('/insert', function(req, res){ //Changed req.query to req.body !
    // simple validation
    if(req.body.resume_name == null) {
        res.send('Resume name must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,resume_id, account_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                //res.redirect(302, '/resume/all');
                account_dal.getById(account_id, function (err, accountResult) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        resume_dal.getById(resume_id, function (err, resumeResult) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('resume/resumeUpdate', {
                                    'result': accountResult[0],
                                    'skills': accountResult[1],
                                    'companies': accountResult[2],
                                    'schools': accountResult[3],
                                    'resume': resumeResult[0],
                                    'was_successful': true
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

// Delete an account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.delete(req.query.resume_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        account_dal.getById(req.query.account_id, function (err, accountResult) {
            if (err) {
                res.send(err);
            }
            else {
                resume_dal.getById(req.query.resume_id, function (err, resumeResult) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('resume/resumeUpdate', {
                            'result': accountResult[0],
                            'skills': accountResult[1],
                            'companies': accountResult[2],
                            'schools': accountResult[3],
                            'resume': resumeResult[0]
                        });
                    }
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    resume_dal.update(req.query, function(err, resume_name){
        //res.render('resume/resumeViewAll', {'resume': resume_id, 'was_successful': true});
        resume_dal.getAll(function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewAll', { 'result':result, 'resume': resume_name, 'was_successful': true });
            }
        });

    });
});

module.exports = router;
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {

    var query = 'CALL account_getinfo(?) ';

    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var account_id;

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?,?,?) ';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function (err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED SKILL_IDs INTO account_skill
        account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ? ';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                accountSkillData.push([account_id, params.skill_id[i]]);
            }
        }
        else {
            accountSkillData.push([account_id, params.skill_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [accountSkillData], function (err, result) {

            // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED COMPANY_IDs INTO account_company

            // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
            query = 'INSERT INTO account_company (account_id, company_id) VALUES ? ';

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var accountCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    accountCompanyData.push([account_id, params.company_id[i]]);
                }
            }
            else {
                accountCompanyData.push([account_id, params.company_id]);
            }

            // NOTE THE EXTRA [] AROUND companyAddressData
            connection.query(query, [accountCompanyData], function (err, result) {

                // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
                query = 'INSERT INTO account_school (account_id, school_id) VALUES ? ';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var accountSchoolData = [];
                if (params.school_id.constructor === Array) {
                    for (var i = 0; i < params.school_id.length; i++) {
                        accountSchoolData.push([account_id, params.school_id[i]]);
                    }
                }
                else {
                    accountSchoolData.push([account_id, params.school_id]);
                }

                // NOTE THE EXTRA [] AROUND companyAddressData
                connection.query(query, [accountSchoolData], function (err, result) {
                    callback(err, result);
                });


            });

        });

    });
};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};
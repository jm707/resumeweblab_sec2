var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM school;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT s.*, a.street, a.zip_code FROM school s ' +
        'LEFT JOIN address a on a.address_id = s.address_id ' +
        'WHERE s.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE SCHOOL
    var query = 'INSERT INTO school (school_name, address_id) VALUES (?)';

    var queryData = [params.school_name, params.address_id];

    connection.query(query, [queryData], function(err, result) {

        callback(err, result);
    });

};

exports.edit = function(school_id, callback) {
    var query = 'SELECT s.school_id, s.school_name, a.street, a.zip_code FROM school s '
    + 'LEFT JOIN address a ON a.address_id = s.address_id '
    + 'WHERE school_id = (?) ';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(school_id, callback) {
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.userGet = function (account_id, callback) {

    var query = 'SELECT s.school_id, s.school_name FROM account a ' +
    'LEFT JOIN account_school sa ON a.account_id = sa.account_id ' +
    'LEFT JOIN school s ON sa.school_id = s.school_id ' +
    'WHERE a.account_id = ? ';

    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
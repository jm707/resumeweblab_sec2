var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT a.account_id, r.resume_id, first_name, last_name, r.resume_name FROM account a ' +
    'LEFT JOIN resume r ON r.account_id = a.account_id ' +
    'WHERE resume_name IS NOT NULL ' +
    'ORDER BY a.first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {

    var query = 'CALL resume_getinfo(?) ';

    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var resume_id;

    // FIRST INSERT THE account
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?,?) ';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, queryData, function (err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED SKILL_IDs INTO resume_skill
        resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ? ';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var resumeSkillData = [];
        if (params.skill_id.constructor === Array) {
            for (var i = 0; i < params.skill_id.length; i++) {
                resumeSkillData.push([resume_id, params.skill_id[i]]);
            }
        }
        else {
            resumeSkillData.push([resume_id, params.skill_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [resumeSkillData], function (err, result) {


            // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
            query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ? ';

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var resumeCompanyData = [];
            if (params.company_id.constructor === Array) {
                for (var i = 0; i < params.company_id.length; i++) {
                    resumeCompanyData.push([resume_id, params.company_id[i]]);
                }
            }
            else {
                resumeCompanyData.push([resume_id, params.company_id]);
            }

            // NOTE THE EXTRA [] AROUND companyAddressData
            connection.query(query, [resumeCompanyData], function (err, result) {

                // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
                query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ? ';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var resumeSchoolData = [];
                if (params.school_id.constructor === Array) {
                    for (var i = 0; i < params.school_id.length; i++) {
                        resumeSchoolData.push([resume_id, params.school_id[i]]);
                    }
                }
                else {
                    resumeSchoolData.push([resume_id, params.school_id]);
                }

                // NOTE THE EXTRA [] AROUND companyAddressData
                connection.query(query, [resumeSchoolData], function (err, result) {
                    callback(err, resume_id, params.account_id);
                });

            });

        });

    });
};

/////////////////////////////////////////////////////////////////////////
//declare the function so it can be used locally
var resumeSkillInsert = function(resume_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var resumeSkillData = []; //like query data but for multiple entries
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < skillIdArray.length; i++) {
            resumeSkillData.push([resume_id, skillIdArray[i]]);
        }
    }
    else {
        resumeSkillData.push([resume_id, skillIdArray]);
    }
    connection.query(query, [resumeSkillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.resumeSkillInsert = resumeSkillInsert;

var resumeCompanyInsert = function (resume_id, companyIdArray, callback) {
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

    var resumeCompanyData = [];
    if (companyIdArray.constructor === Array) {
        for (var i = 0; i < companyIdArray.length; i++) {
            resumeCompanyData.push([resume_id, companyIdArray[i]]);
        }
    }
    else {
        resumeCompanyData.push([resume_id, companyIdArray]);
    }
    connection.query(query, [resumeCompanyData], function(err, result){
        callback(err, result);
    });
};

module.exports.resumeCompanyInsert = resumeCompanyInsert;

var resumeSchoolInsert = function (resume_id, schoolIdArray, callback) {
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

    var resumeSchoolData = [];
    if (schoolIdArray.constructor === Array) {
        for (var i = 0; i < schoolIdArray.length; i++) {
            resumeSchoolData.push([resume_id, schoolIdArray[i]]);
        }
    }
    else {
        resumeSchoolData.push([resume_id, schoolIdArray]);
    }
    connection.query(query, [resumeSchoolData], function(err, result){
        callback(err, result);
    });


};

module.exports.resumeSchoolInsert = resumeSchoolInsert;
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//declare the function so it can be used locally
var resumeSkillDeleteAll = function(resume_id, callback){
    var query = 'DELETE FROM resume_skill WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.resumeSkillDeleteAll = resumeSkillDeleteAll;

var resumeCompanyDeleteAll = function (resume_id, callback) {
    var query = 'DELETE FROM resume_company WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

module.exports.resumeCompanyDeleteAll = resumeCompanyDeleteAll;

var resumeSchoolDeleteAll = function (resume_id, callback) {
    var query = 'DELETE FROM resume_school WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

module.exports.resumeSchoolDeleteAll = resumeSchoolDeleteAll;
///////////////////////////////////////////////////////////////////////

exports.update = function (params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {

        //delete resume_skill entries for this resume
        resumeSkillDeleteAll(params.resume_id, function(err, result){

            if(params.skill_id != null) {
                //insert resume_skill ids
                resumeSkillInsert(params.resume_id, params.skill_id, function (err, result) {
                   if (err) {
                       res.send(err);
                   }
                });
            }
        });

        //delete resume_company entries for this resume
        resumeCompanyDeleteAll(params.resume_id, function (err, result) {

            if(params.company_id != null) {
                //insert resume_company ids
                resumeCompanyInsert(params.resume_id, params.company_id, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                });
            }
        });

        //delete resume_school entries for this resume
        resumeSchoolDeleteAll(params.resume_id, function (err, result) {

            if (params.school_id != null) {
                //insert resume_school ids
                resumeSchoolInsert(params.resume_id, params.school_id, function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                });
            }

            else {
                callback(err, params.resume_name);
            }

        });

        callback(err, params.resume_name);
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.edit = function (resume_id, callback) {

};